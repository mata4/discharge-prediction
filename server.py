#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# Import libraries
import numpy as np
from flask import Flask, request, jsonify
import pickle
app = Flask(__name__)

app = Flask(__name__,template_folder = r"C:\Users\mikhajanemata\Documents\Theoria\AI 57_Discharge Prediction") 
# In[ ]:


# Load the model
model = pickle.load(open('dischargeModel.pkl','rb'))
countVector = pickle.load(open('count_vect.pkl','rb'))

@app.route('/api',methods=['POST'])

#@app.route('/result',methods = ['POST', 'GET'])

def predict():
    # Get the data from the POST request.
    progressNotes = request.get_json(force=True)
    # Make prediction using model loaded from disk as per the data.
    prediction = model.predict(countVector.transform(['progressNotes']))
    # Take the first value of prediction
    output = prediction[0]
    return jsonify(output)


if __name__ == '__main__':
    app.run(port=5000, debug=True)
